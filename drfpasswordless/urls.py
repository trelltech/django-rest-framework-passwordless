from django.urls import path
from .views import (ObtainEmailCallbackToken,
                    ObtainMobileCallbackToken,
                    ObtainAuthTokenFromCallbackToken,
                    VerifyAliasFromCallbackToken,
                    ObtainEmailVerificationCallbackToken,
                    ObtainMobileVerificationCallbackToken,
                    )

urlpatterns = [path(r'callback/auth/', ObtainAuthTokenFromCallbackToken.as_view(), name='auth_callback'),
               path(r'auth/email/', ObtainEmailCallbackToken.as_view(), name='auth_email'),
               path(r'auth/mobile/', ObtainMobileCallbackToken.as_view(), name='auth_mobile'),
               path(r'callback/verify/', VerifyAliasFromCallbackToken.as_view(), name='verify_callback'),
               path(r'verify/email/', ObtainEmailVerificationCallbackToken.as_view(), name='verify_email'),
               path(r'verify/mobile/', ObtainMobileVerificationCallbackToken.as_view(), name='verify_mobile')]
